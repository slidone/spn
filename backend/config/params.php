<?php
return [
    'adminEmail' => 'admin@example.com',
    'uploadsRoot' => 'http://spn.local/uploads/',
    'frontendUrl' => 'http://spn.local/',
    'appVersion' => '0.01a'
];
