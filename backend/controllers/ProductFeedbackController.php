<?php

namespace backend\controllers;

use common\models\Product;
use common\models\UploadImageForm;
use Yii;
use common\models\ProductFeedback;
use common\models\search\ProductFeedbackSearch;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductFeedbackController implements the CRUD actions for ProductFeedback model.
 */
class ProductFeedbackController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'view', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductFeedback models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductFeedbackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductFeedback model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->checkParam($id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductFeedback model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param $productId
     * @return mixed
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionCreate($productId)
    {
        $this->checkParam($productId);
        if (!$productId || !is_numeric($productId) || $productId < 0 )
        {
            throw new BadRequestHttpException('Not current Product ID');
        }

        if (!Product::findOne(['id' => $productId]))
        {
            throw new NotFoundHttpException('Not found Product with that ID');
        }
        $model = new ProductFeedback();
        $post = Yii::$app->request->post();
        $uploadForm = new UploadImageForm();
        
        if ($model->load($post)) {
            $model->product_id = $productId;
            if ($model->save())
            {
                $this->uploadUserAvatar($uploadForm, $model);
                return $this->redirect(['/product/view', 'id' => $model->product_id]);
            }

        }

        return $this->render('create', [
            'model' => $model,
            'uploadForm' => $uploadForm,
        ]);
    }

    /**
     * Updates an existing ProductFeedback model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->checkParam($id);
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        $uploadForm = new UploadImageForm();

        if ($model->load($post)) {
            if ($model->save())
            {
                $this->uploadUserAvatar($uploadForm, $model);
                return $this->redirect(['/product/view', 'id' => $model->product_id]);
            }

        }

        return $this->render('create', [
            'model' => $model,
            'uploadForm' => $uploadForm,
        ]);
    }

    /**
     * Deletes an existing ProductFeedback model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->checkParam($id);
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the ProductFeedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductFeedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductFeedback::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function uploadUserAvatar(UploadImageForm $uploadForm, ProductFeedback $model)
    {
        $file = UploadedFile::getInstance($uploadForm,'file');
        if ($file)
        {
            $model->saveUserAvatar($uploadForm->uploadFile($file, $model->user_avatar, 'avatars/product-feedback/'));
        }
    }

    private function checkParam($id)
    {
        if (!is_numeric($id) || $id < 0)
        {
            throw new NotFoundHttpException('Page not found');
        }
    }
}
