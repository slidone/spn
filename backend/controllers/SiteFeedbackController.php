<?php

namespace backend\controllers;

use common\models\UploadImageForm;
use Yii;
use common\models\SiteFeedback;
use common\models\search\SiteFeedbackSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SiteFeedbackController implements the CRUD actions for SiteFeedback model.
 */
class SiteFeedbackController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'view', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SiteFeedback models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiteFeedbackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SiteFeedback model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->checkParam($id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SiteFeedback model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SiteFeedback();
        $uploadForm = new UploadImageForm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) 
        {
            $file = UploadedFile::getInstance($uploadForm,'file');
            if ($model->saveUserAvatar($uploadForm->uploadFile($file, $model->user_avatar, 'avatars/feedback/')))
            {
                return $this->redirect(['view', 'id' => $model->id]);
            }

        }

        return $this->render('create', [
            'model' => $model,
            'uploadForm' => $uploadForm,
        ]);
    }

    /**
     * Updates an existing SiteFeedback model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->checkParam($id);
        $model = $this->findModel($id);
        $uploadForm = new UploadImageForm;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $file = UploadedFile::getInstance($uploadForm,'file');
            if ($file)
            {
                $model->saveUserAvatar($uploadForm->uploadFile($file, $model->user_avatar, 'avatars/feedback/'));
            }
            return $this->redirect(['view', 'id' => $model->id]);

        }

        return $this->render('update', [
              'model' => $model,
              'uploadForm' => $uploadForm,
        ]);
    }

    /**
     * Deletes an existing SiteFeedback model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->checkParam($id);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SiteFeedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SiteFeedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SiteFeedback::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function checkParam($id)
    {
        if (!is_numeric($id) || $id < 0)
        {
            throw new NotFoundHttpException('Page not found');
        }
    }
}
