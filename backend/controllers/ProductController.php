<?php

namespace backend\controllers;

use common\models\Attribute;
use common\models\ProductFeedback;
use common\models\UploadImageForm;
use common\models\Value;
use Yii;
use common\models\Product;
use common\models\search\ProductSearch;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'view', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->checkParam($id);
        $model = $this->findModel($id);

        $dataProviderFeedback = new ActiveDataProvider([
            'query' => ProductFeedback::find()->where(['product_id' => $id]),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('view', [
            'model' => $model,
            'items' => $this->getGallery($model->gallery),
            'dataProviderFeedback' => $dataProviderFeedback,
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product;
        $uploadForm = new UploadImageForm();
        $values = $this->initValues($model);
        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save() && Model::loadMultiple($values, $post))
        {
            $this->processValues($values, $model);
            $this->uploadMainImage($uploadForm, $model);
            $this->uploadGallery($uploadForm, $model);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'uploadForm' => $uploadForm,
            'values' => $values,
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->checkParam($id);
        $model = $this->findModel($id);
        $uploadForm = new UploadImageForm;
        $values = $this->initValues($model);

        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save() && Model::loadMultiple($values, $post))
        {
            $this->processValues($values, $model);
            $this->uploadMainImage($uploadForm, $model);
            $this->uploadGallery($uploadForm, $model);

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'uploadForm' => $uploadForm,
            'gallery' => $this->getGallery($model->gallery),
            'values' => $values,
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->checkParam($id);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $this->checkParam($id);
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param Product $model
     * @return Value[]
     */
    private function initValues(Product $model)
    {
        /** @var Value[] $values */
        $values = $model->getValues()->with('productAttribute')->indexBy('attribute_id')->all();
        $attributes = Attribute::find()->indexBy('id')->all();
        foreach (array_diff_key($attributes, $values) as $attribute) {
            $values[$attribute->id] = new Value(['attribute_id' => $attribute->id]);
        }
        foreach ($values as $value) {
            $value->setScenario(Value::SCENARIO_TABULAR);
        }
        return $values;
    }
    /**
     * @param Value[] $values
     * @param Product $model
     */
    private function processValues($values, Product $model)
    {
        foreach ($values as $value) {
            $value->product_id = $model->id;
            if ($value->validate()) {
                if (!empty($value->value)) {
                    $value->save(false);
                } else {
                    $value->delete();
                }
            }
        }
    }

    private function uploadMainImage(UploadImageForm $uploadForm, Product $model)
    {
        $file = UploadedFile::getInstance($uploadForm,'file');
        if ($file)
        {
            $model->saveMainImage($uploadForm->uploadFile($file, $model->main_image, 'products/'. $model->id . '/main_image/'));
        }
    }

    private function uploadGallery(UploadImageForm $uploadForm, Product $model)
    {
        $files = UploadedFile::getInstances($uploadForm, 'files');
        if($files)
        {
            $model->saveGallery($uploadForm->uploadFiles($files, unserialize($model->gallery), 'products/'. $model->id . '/gallery/'));
        }
    }

    private function getGallery($pgallery)
    {
        $gallery = unserialize($pgallery);
        if ($gallery && !is_null($gallery))
        {
            $items = [];
            foreach ($gallery as $item => $value)
            {
                $newItem = Yii::$app->params['uploadsRoot'] . $value;
                array_push($items,$newItem);
            }
        }
        return $items;
    }

    private function checkParam($id)
    {
        if (!is_numeric($id) || $id < 0)
        {
            throw new NotFoundHttpException('Page not found');
        }
    }
}
