<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SiteFeedback */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Site Feedback',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Site Feedbacks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="site-feedback-update">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-body">
                            <?= $this->render('_form', [
                                'model' => $model,
                                'uploadForm' => $uploadForm,
                            ]) ?>
                        </div>
                    </div>
                </div>
        </section>
</div>