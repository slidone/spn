<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SiteFeedback */

$this->title = 'View ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Site Feedbacks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-feedback-view">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header ">
                            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </div>
                        <div class="box-body">
                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'username',
                                    [
                                        'attribute' => 'user_avatar',
                                        'format'=>'raw',
                                        'value' => \yii\bootstrap\Html::img(Yii::$app->params['uploadsRoot'] . $model->user_avatar),
                                    ],
                                    'address',
                                    'text:html',
                                    [
                                        'attribute' => 'isActive',
                                        'format'=>'raw',
                                        'value'=>$model->isActive ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>'
                                    ],
                                    'created_at:datetime',
                                    'updated_at:datetime',
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
</div>
