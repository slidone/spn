<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SiteFeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Site Feedbacks');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <?= Html::a(Yii::t('app', 'Create Site Feedback'), ['create'], ['class' => 'btn btn-success']) ?>
                </div>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'options' => ['class' => 'box-body table-responsive no-padding'],
                    'footerRowOptions' => ['class' => 'box-footer clearfix'],
                    'layout'=>"{items}",
                    'tableOptions' => [
                        'class' => 'table table-hover table-striped'
                    ],
                    'columns' => [
                        'id',
                        [
                            'attribute' => 'username',
                            'content' => function($data){
                                return Html::a($data->username, \yii\helpers\Url::to(['view', 'id' => $data->id]));
                            }
                        ],
                        [
                            'attribute' => 'isActive',
                            'filter' => [0 => 'No', 1 => 'Yes'],
                            'format' => 'boolean',
                            'content' => function($data){
                                return $data->isActive ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>';
                            }
                        ],
                        'created_at:datetime',
                        'updated_at:datetime',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update} {delete}{link}',
                            'buttons' => [
                                'update' => function ($url,$model) {
                                    return Html::a(
                                        '<span class="label label-warning"><i class="fa fa-pencil"></i> Edit</span>',
                                        $url);
                                },
                                'delete' => function ($url,$model) {
                                    return Html::a(
                                        '<span class="label label-danger"><i class="fa fa-trash"></i> Delete</span>',
                                        $url,[
                                        'title' => Yii::t('yii', 'Delete'),
                                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this feedback?'),
                                        'data-method' => 'post',
                                        'data-pjax' => '0',
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
</section>