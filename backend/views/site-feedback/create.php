<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SiteFeedback */

$this->title = Yii::t('app', 'Create Site Feedback');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Site Feedbacks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-feedback-create">
    <div class="site-feedback-create">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-body">
                            <?= $this->render('_form', [
                                'model' => $model,
                                'uploadForm' => $uploadForm,
                            ]) ?>
                        </div>
                    </div>
                </div>
        </section>
    </div>
</div>
