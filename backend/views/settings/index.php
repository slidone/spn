<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-index">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <?= Html::a(Yii::t('app', 'Add Settings'), ['create'], ['class' => 'btn btn-success']) ?>
                    </div>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $searchModel,
                        'options' => ['class' => 'box-body table-responsive no-padding'],
                        'footerRowOptions' => ['class' => 'box-footer clearfix'],
                        'layout'=>"{items} {summary}",
                        'summaryOptions' => ['class' => 'box-footer'],
                        'tableOptions' => [
                            'class' => 'table table-hover table-striped'
                        ],
                        'columns' => [
                            //'description',
                            'key',
                            'value',
                            [
                                'attribute' => 'r_flag',
                                'content' => function($data)
                                {
                                    return !$data->r_flag ? '<span class="text-danger">System setting</span>' :  '<span class="text-info">Custom setting </span>';
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update} {delete}{link}',
                                'buttons' => [
                                    'update' => function ($url,$model) {
                                        return Html::a(
                                            '<span class="label label-warning"><i class="fa fa-pencil"></i> Edit</span>',
                                            $url);
                                    },
                                    'delete' => function ($url,$model) {
                                        return $model->r_flag ? Html::a(
                                            '<span class="label label-danger"><i class="fa fa-trash"></i> Delete</span>',
                                            $url) : '';
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </section>
</div>
