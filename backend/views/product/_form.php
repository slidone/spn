<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $uploadForm common\models\UploadImageForm */
/* @var $form yii\widgets\ActiveForm */

if ($model->isNewRecord)
{
    $initPreview = '';
}
else
{
    $initPreview = $model->gallery ? $gallery : '';
}
?>
<div class="product-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'category_id')->dropDownList(\common\models\Category::find()->select(['title', 'id'])->indexBy('id')->column()) ?>

            <?= $form->field($model, 'price')->textInput() ?>

            <?= $form->field($model, 'description')->widget(\dosamigos\ckeditor\CKEditor::className(), [
                'options' => ['rows' => 10],
                'preset' => 'basic'
            ]) ?>

            <?= $form->field($uploadForm, 'file')->widget(FileInput::classname(), [
                'pluginOptions' => [
                    'previewFileType' => 'image',
                    'initialPreview'=> [
                        $model->main_image ? Yii::$app->params['uploadsRoot'] . $model->main_image : null
                    ],
                    'initialPreviewAsData'=> true,
                    'showCaption' => false,
                    'showRemove' => false,
                    'showUpload' => false,
                    'browseClass' => 'btn btn-primary btn-block',
                    'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                    'browseLabel' =>  'Select Photo'
                ],
                'options' => ['accept' => 'image/*'],
            ]);
            ?>

            <?= $form->field($uploadForm, 'files[]')->widget(FileInput::classname(), [
                'options' => [
                    'accept' => 'image/*',
                    'multiple' => true
                ],
                'pluginOptions' => [
                    'previewFileType' => 'image',
                    'showRemove' => false,
                    'showUpload' => false,
                    'initialPreview'=> $initPreview ,
                    'initialPreviewAsData'=> true,
                    'initialCaption'=>"Gallery Images",
                    'uploadUrl' => \yii\helpers\Url::to(['/product/upload']),
                    'uploadExtraData' => [
                        'id' => 20,
                    ],
                    'deleteUrl' => \yii\helpers\Url::to(['/product/upload']),
                ]
            ]);
            ?>

            <?= $form->field($model, 'showSlider')->checkbox() ?>

            <?= $form->field($model, 'isAvailable')->checkbox() ?>

            <?= $form->field($model, 'isPopular')->checkbox() ?>
        </div>
        <div class="col-md-4">
                    <?php foreach ($values as $value): ?>
                        <?= $form->field($value, '[' . $value->productAttribute->id . ']value')->label($value->productAttribute->name); ?>
                    <?php endforeach; ?>
        </div>
    </div>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
