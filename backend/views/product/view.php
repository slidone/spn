<?php

use yii\helpers\Html;
//use yii\widgets\DetailView;
use kartik\detail\DetailView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Actions</h3>
                    </div>
                    <div class="box-body">
                        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Main info</h3>
                            </div>
                            <div class="box-body">
                                <?= DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [
                                        'title',
                                        'price',
                                        'description:html',
                                        'category_id',
                                        //'main_image',
                                        //'gallery',
                                        'showSlider:boolean',
                                        'isAvailable:boolean',
                                        'isPopular:boolean',
                                        'created_at:datetime',
                                        'updated_at:datetime',
                                    ],
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Feedback`s product</h3>
                            </div>
                            <div class="box-body">
                                
                                <?= \yii\widgets\ListView::widget([
                                    'dataProvider' => $dataProviderFeedback,
                                    'itemView' => '_feedback',
                                    'layout' => "{items}\n{pager}",
                                ]);?>
                            </div>
                            <div class="box-footer text-center">
                                <?= Html::a(
                                    '<i class="fa fa-plus"></i> Add feedback',
                                    Url::to(['/product-feedback/create', 'productId' => $model->id]),
                                        [
                                            'class'=>'btn btn-block btn-success'
                                        ]
                                    )?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Attributes</h3>
                            </div>
                            <div class="box-body">
                                <?= \yii\grid\GridView::widget([
                                    'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $model->getValues()]),
                                    'layout' => "{items}\n{pager}",
                                    'columns' => [
                                        [
                                            'attribute' => 'attribute_id',
                                            'value' => 'productAttribute.name',
                                        ],
                                        'value',
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Gallery</h3>
                            </div>
                            <div class="box-body text-center">
                                <?if($items):?>
                                    <?= dosamigos\gallery\Carousel::widget([
                                        'items' => $items,
                                    ]);?>
                                <?else:?>
                                    <i>Not found images for this product.</i>
                                <?endif;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>