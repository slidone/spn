<?
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
/* @var $model common\models\ProductFeedback */
?>
<div class="post">
    <div class="user-block">
        <?= Html::img(Yii::$app->params['uploadsRoot'].$model->user_avatar, ['class'=> 'img-circle img-bordered-sm'])?>
        <span class="username">
            <?= Html::a(
                Html::encode($model->username),
                Url::to([
                    '/product-feedback/view',
                    'id' => $model->id
                ]))?>
            <?= Html::a(
                '<i class="fa fa-times "></i> Delete',
                Url::to([
                    '/product-feedback/delete',
                    'id' => $model->id
                ]),
                [
                    'class' => 'pull-right btn-box-tool link-black text-sm',
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this feedback?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                ])?>
            <?= Html::a(
                '<i class="fa fa-pencil "></i> Update',
                Url::to([
                    '/product-feedback/update',
                    'id' => $model->id
                ]),
                [
                    'class' => 'pull-right btn-box-tool link-black text-sm',
                ])?>
        </span>
        <span class="description">
            <?echo $model->isActive ? '<span class="text-success"> Active </span>' : '<span class="text-warning"> Hide </span>'?>
        </span>
    </div>
    <p><?= HtmlPurifier::process($model->text) ?></p>
</div>
<hr>