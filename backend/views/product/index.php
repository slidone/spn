<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <?= Html::a(Yii::t('app', 'Create Product'), ['create'], ['class' => 'btn btn-success']) ?>
                    </div>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'options' => ['class' => 'box-body table-responsive no-padding'],
                        'footerRowOptions' => ['class' => 'box-footer clearfix'],
                        'layout'=>"{items}",
                        'tableOptions' => [
                            'class' => 'table table-hover table-striped'
                        ],
                        'columns' => [
                            [
                                'attribute' => 'main_image',
                                'filter' => '',
                                'content' => function($data){
                                    return $data->main_image ? Html::img(Yii::$app->params['uploadsRoot'].$data->main_image, ['style'=>'height:50px'])  : null;
                                },
                            ],
                            [
                                'attribute' => 'title',
                                'content' => function($data){
                                    return Html::a($data->title,\yii\helpers\Url::to(['view', 'id' => $data->id]));
                                },
                            ],
                            [
                                'attribute' => 'category_id',
                                'filter' => \common\models\Category::find()->select(['title', 'id'])->indexBy('id')->column(),
                                'value' => 'category.title',
                            ],
                            'price',
                            [
                                'attribute' => 'showSlider',
                                'filter' => [0 => 'No', 1 => 'Yes'],
                                'format' => 'boolean',
                                'content' => function($data){
                                    return $data->showSlider ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>';
                                }
                            ],
                            [
                                'attribute' => 'isAvailable',
                                'filter' => [0 => 'No', 1 => 'Yes'],
                                'format' => 'boolean',
                                'content' => function($data){
                                    return $data->isAvailable ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>';
                                }
                            ],
                            [
                                'attribute' => 'isPopular',
                                'filter' => [0 => 'No', 1 => 'Yes'],
                                'format' => 'boolean',
                                'content' => function($data){
                                    return $data->isPopular ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>';
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update} {delete}{link}',
                                'buttons' => [
                                    'update' => function ($url,$model) {
                                        return Html::a(
                                            '<span class="label label-warning"><i class="fa fa-pencil"></i> Edit</span>',
                                            $url);
                                    },
                                    'delete' => function ($url,$model) {
                                        return Html::a(
                                            '<span class="label label-danger"><i class="fa fa-trash"></i> Delete</span>',
                                            $url,[
                                            'title' => Yii::t('yii', 'Delete'),
                                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this product?'),
                                            'data-method' => 'post',
                                            'data-pjax' => '0',
                                        ]);
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </section>
</div>
