<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">APP</span><span class="logo-lg">' . Yii::$app->settings->siteName . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <li>
                    <?= \yii\bootstrap\Html::a('<i class="fa fa-sign-out"></i> '.Yii::$app->user->identity->username.' (Logout)',['/site/logout'])?>
                </li>
            </ul>
        </div>
    </nav>
</header>
