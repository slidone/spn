<aside class="main-sidebar">

    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'General', 'options' => ['class' => 'header']],
                    ['label' => 'Dashboard', 'icon' => 'dashboard', 'url' => ['/site/index']],
                    ['label' => 'Settings', 'icon' => 'cog', 'url' => ['/settings']],
                    //['label' => 'Users', 'icon' => 'user', 'url' => ['/users']],
                    //['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                    ['label' => 'Content management', 'options' => ['class' => 'header']],
                    ['label' => 'Categories', 'icon' => 'hashtag', 'url' => ['/category']],
                    ['label' => 'Products', 'icon' => 'file', 'url' => ['/product']],
                    ['label' => 'Products Feedback`s', 'icon' => 'comment-o', 'url' => ['/product-feedback']],
                    ['label' => 'Attributes', 'icon' => 'sliders', 'url' => ['/attribute']],
                    //['label' => 'Orders', 'icon' => 'shopping-bag', 'url' => ['/order']],

                    ['label' => 'Others', 'options' => ['class' => 'header']],
                    ['label' => 'Site Feedback`s', 'icon' => 'comment', 'url' => ['/site-feedback']],
                    ['label' => 'Static Pages', 'icon' => 'file', 'url' => ['/pages']],

                    //['label' => 'System Function', 'options' => ['class' => 'header']],
                    //['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    //['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                ],
            ]
        ) ?>

    </section>

</aside>
