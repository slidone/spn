<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Attributes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attribute-index">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <?= Html::a(Yii::t('app', 'Create Attribute'), ['create'], ['class' => 'btn btn-success']) ?>
                    </div>
                    <?= GridView::widget([
                        'options' => ['class' => 'box-body table-responsive no-padding'],
                        'footerRowOptions' => ['class' => 'box-footer clearfix'],
                        'layout'=>"{items}",
                        'tableOptions' => [
                            'class' => 'table table-hover table-striped'
                        ],
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            'name',
                            'type',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update} {delete}{link}',
                                'buttons' => [
                                    'update' => function ($url,$model) {
                                        return Html::a(
                                            '<span class="label label-warning"><i class="fa fa-pencil"></i> Edit</span>',
                                            $url);
                                    },
                                    'delete' => function ($url,$model) {
                                        return Html::a(
                                            '<span class="label label-danger"><i class="fa fa-trash"></i> Delete</span>',
                                            $url,[
                                            'title' => Yii::t('yii', 'Delete'),
                                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this attribute?'),
                                            'data-method' => 'post',
                                            'data-pjax' => '0',
                                        ]);
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </section>
</div>
