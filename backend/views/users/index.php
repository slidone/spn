<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <?= Html::a(Yii::t('app', 'Add user'), ['create'], ['class' => 'label label-success']) ?>
                    </div>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $searchModel,
                        'options' => ['class' => 'box-body table-responsive no-padding'],
                        'footerRowOptions' => ['class' => 'box-footer clearfix'],
                        'layout'=>"{items} {summary}",
                        'summaryOptions' => ['class' => 'box-footer'],
                        'tableOptions' => [
                            'class' => 'table table-hover table-striped'
                        ],
                        'columns' => [
                            'username',
                            'email:email',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update} {delete}{link}',
                                'buttons' => [
                                    'update' => function ($url,$model) {
                                        return Html::a(
                                            '<span class="label label-warning"><i class="fa fa-pencil"></i> Edit</span>',
                                            $url);
                                    },
                                    'delete' => function ($url,$model) {
                                        return $model->id != 1 ? Html::a(
                                            '<span class="label label-danger"><i class="fa fa-trash"></i> Delete</span>',
                                            $url) : '';
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </section>
</div>