<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductFeedback */
/* @var $uploadForm common\models\UploadImageForm */
$this->title = Yii::t('app', 'Create Product Feedback');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Product Feedbacks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <?= $this->render('_form', [
                            'model' => $model,
                            'uploadForm' => $uploadForm,
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>