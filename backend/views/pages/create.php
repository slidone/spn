<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StaticPages */

$this->title = Yii::t('app', 'Create Static Pages');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Static Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="static-pages">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <?= $this->render('_form', [
                            'model' => $model,
                        ]) ?>
                    </div>
                </div>
            </div>
    </section>
</div>