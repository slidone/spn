<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\StaticPagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Static Pages');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <?= Html::a(Yii::t('app', 'Create Static Pages'), ['create'], ['class' => 'btn btn-success']) ?>
                </div>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'options' => ['class' => 'box-body table-responsive no-padding'],
                    'footerRowOptions' => ['class' => 'box-footer clearfix'],
                    'layout'=>"{items}",
                    'tableOptions' => [
                        'class' => 'table table-hover table-striped'
                    ],
                    'columns' => [
                        //'id',

                        [
                            'attribute' => 'name',
                            'format' => 'raw',
                            'content' => function($data){
                                return '<a target=_blank href="'.
                                Yii::$app->params['frontendUrl'].
                                \yii\helpers\Url::to(['site/page', 'url' => $data->short_url]).'">'.
                                $data->name.'</a>';
                            }
                        ],
                        'short_url',
                        [
                            'attribute' => 'isActive',
                            'filter' => [0 => 'No', 1 => 'Yes'],
                            'format' => 'boolean',
                            'content' => function($data){
                                return $data->isActive ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>';
                            }
                        ],
                        'position',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update} {delete}{link}',
                            'buttons' => [
                                'update' => function ($url,$model) {
                                    return Html::a(
                                        '<span class="label label-warning"><i class="fa fa-pencil"></i> Edit</span>',
                                        $url);
                                },
                                'delete' => function ($url,$model) {
                                    return Html::a(
                                        '<span class="label label-danger"><i class="fa fa-trash"></i> Delete</span>',
                                        $url,[
                                        'title' => Yii::t('yii', 'Delete'),
                                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this category?'),
                                        'data-method' => 'post',
                                        'data-pjax' => '0',
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
</section>