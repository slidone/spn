<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\ElFinder;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model common\models\StaticPages */
/* @var $form yii\widgets\ActiveForm */

$position = [
    ['pos' => 'header', 'value' => 'Header menu'],
    ['pos' => 'footer', 'value' => 'Footer menu'],
];
?>

<div class="static-pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'options' => ['rows' => 10],
        'preset' => 'full',
        'clientOptions' => ElFinder::ckeditorOptions('elfinder',['language'=> 'en']),
    ])?>

    <?= $form->field($model, 'short_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'position')->dropDownList(\yii\helpers\ArrayHelper::map($position, 'pos', 'value'))?>

    <?= $form->field($model, 'isActive')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
