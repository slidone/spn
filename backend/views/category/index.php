<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <?= Html::a(Yii::t('app', 'Create Category'), ['create'], ['class' => 'btn btn-success']) ?>
                </div>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'options' => ['class' => 'box-body table-responsive no-padding'],
                    'footerRowOptions' => ['class' => 'box-footer clearfix'],
                    'layout'=>"{items}",
                    'tableOptions' => [
                        'class' => 'table table-hover table-striped'
                    ],
                    'columns' => [
                        [
                            'attribute' => 'title',
                            'content' => function($data){
                                return Html::a($data->title, \yii\helpers\Url::to(['view', 'id' => $data->id]));
                            }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update} {delete}{link}',
                            'buttons' => [
                                'update' => function ($url,$model) {
                                    return Html::a(
                                        '<span class="label label-warning"><i class="fa fa-pencil"></i> Edit</span>',
                                        $url);
                                },
                                'delete' => function ($url,$model) {
                                    return Html::a(
                                        '<span class="label label-danger"><i class="fa fa-trash"></i> Delete</span>',
                                        $url,[
                                        'title' => Yii::t('yii', 'Delete'),
                                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this category?'),
                                        'data-method' => 'post',
                                        'data-pjax' => '0',
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
</section>