<?php

use yii\db\Migration;

/**
 * Handles the creation of table `static_pages`.
 */
class m170531_151942_create_static_pages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('static_pages', [
            'id' => $this->primaryKey(),
            'name' => $this->string(200)->notNull(),
            'content' => $this->text()->notNull(),
            'short_url' => $this->string(200)->notNull(),
            'isActive' => $this->boolean()->defaultValue(false)->notNull(),
        ]);

        $this->insert('static_pages',[
            'name' => 'About us',
            'content' => 'Example page',
            'short_url' => 'about',
            'isActive' => true,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('static_pages');
    }
}
