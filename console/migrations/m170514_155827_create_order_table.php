<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m170514_155827_create_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'firstname' => $this->string(50)->notNull(),
            'lastname' => $this->string(50)->notNull(),
            'email' => $this->string(50)->notNull(),
            'phone' => $this->integer(12)->notNull(),
            'address' => $this->string(200)->notNull(),
            'country' => $this->string(50)->notNull(),
            'state'=> $this->string(50)->notNull(),
            'zip'=> $this->string(10)->notNull(),
        ]);

        $this->createTable('{{%order_products}}', [
            'product_id' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('pk-order-products', '{{%order_products}}', ['product_id', 'order_id']);

        $this->createIndex('idx-order_products-product_id', '{{%order_products}}', 'product_id');
        $this->createIndex('idx-order_products-order_id', '{{%order_products}}', 'order_id');

        $this->addForeignKey('fk-order_products-product', '{{%order_products}}', 'product_id', '{{%product}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-order_products-order', '{{%order_products}}', 'order_id', '{{%order}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('order');
    }
}
