<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_feedback`.
 */
class m170514_145754_create_product_feedback_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%product_feedback}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(50)->notNull(),
            'user_avatar' => $this->string(),
            'text' => $this->text()->notNull(),
            'product_id' => $this->integer(),
            'isActive' => $this->boolean()->defaultValue(0)->notNull(),
        ]);

        $this->createIndex('idx-product-feedback-product-id', '{{%product_feedback}}', 'product_id');
        $this->createIndex('idx-product-feedback-active', '{{%product_feedback}}', 'isActive');

        $this->addForeignKey('FK_product_f_product', '{{%product_feedback}}', 'product_id', '{{%product}}', 'id', 'SET NULL');

        $this->insert('{{%product_feedback}}', [
            'username' => 'shaggy',
            'text' => 'I love my DWA 9100, I use it every day when I get home from work. It touches all the spots that are tense & relaxes me to the point that I fall asleep while it\'s massaging',
        ]);

        $this->insert('{{%product_feedback}}', [
            'username' => 'scooby doo',
            'text' => 'I love my DWA 9100, I use it every day when I get home from work. It touches all the spots that are tense & relaxes me to the point that I fall asleep while it\'s massaging',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex('idx-product-feedback-active', '{{%product_feedback}}');
        $this->dropIndex('idx-product-feedback-product-id', '{{%product_feedback}}');
        $this->dropForeignKey('FK_product_f_product', '{{%product_feedback}}');
        $this->dropTable('{{%product_feedback}}');
    }
}
