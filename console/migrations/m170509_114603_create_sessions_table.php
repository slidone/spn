<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sessions`.
 */
class m170509_114603_create_sessions_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('sessions', [
            'id' => 'CHAR(40) NOT NULL PRIMARY KEY',
            'expire' => $this->integer(),
            'data' => 'BLOB',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('sessions');
    }
}
