<?php

use yii\db\Migration;

/**
 * Handles the creation of table `position_column_to_static_pages`.
 */
class m170531_180716_create_position_column_to_static_pages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('static_pages','position', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('static_pages','position');
    }
}
