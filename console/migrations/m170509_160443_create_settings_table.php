<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m170509_160443_create_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string(200)->notNull(),
            'value' => $this->string(),
            'r_flag' => $this->boolean()->defaultValue(true),
        ]);

        $this->insert('settings',[
            'key' => 'siteName',
            'value' => 'Stop Pain Now',
            'r_flag' => false,
        ]);

        $this->insert('settings',[
            'key' => 'metaTags',
            'value' => 'massager, zalupe',
            'r_flag' => false,
        ]);

        $this->insert('settings',[
            'key' => 'indexTextHeader',
            'value' => 'GET INSTANT RELIEF IN THE COMFORT OF YOUR OWN HOME',
            'r_flag' => false,
        ]);
        
        $this->insert('settings',[
            'key' => 'indexTextText',
            'value' => 'The HiDow products employ the latest in TENS (Transcutaneous Electrical Nerve Stimulation) and EMS (Electrical Muscle Stimulation) technology. They are easy to use, compact and have a fully rechargeab',
            'r_flag' => false,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }
}
