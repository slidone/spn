<?php

use yii\db\Migration;

/**
 * Handles the creation of table `site_feedback`.
 */
class m170514_055044_create_site_feedback_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('site_feedback', [
            'id' => $this->primaryKey(),
            'username' => $this->string(50)->notNull(),
            'user_avatar' => $this->string(),
            'address' => $this->string(50)->notNull(),
            'text' => $this->text()->notNull(),
            'isActive' => $this->boolean()->defaultValue(1)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->insert('site_feedback',[
            'username' => 'SHARON',
            'user_avatar' => '',
            'address' => 'ORLANDO, FL',
            'text' => 'I have to tell you how much I LOVE my Massage Mouse. I got one about 8 years ago at a women’s show in Orlando. My doctor gave me a tens unit but my Massage Mouse works even better. I have even given 2 as gifts for people with pain issues. I have lower back problems and a year ago when I had to pack my entire house by myself to move I used the unit every night because of severe pain in my lower back. I don’t think I would have made it through that without my mouse.',
            'isActive' => true,
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ]);

        $this->insert('site_feedback',[
            'username' => 'SUZANNE',
            'user_avatar' => '',
            'address' => 'ATLANTA, GA',
            'text' => 'Thanks for making this life-altering device available to the masses! To be sure, the Hi-Dow XP Micro Massager is an ABSOLUTE must-have for pain management that can go a LONG way towards improving QUALITY OF LIFE!',
            'isActive' => true,
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ]);

        $this->insert('site_feedback',[
            'username' => 'ALEXANDER',
            'user_avatar' => '',
            'address' => 'CHICAGO, IL',
            'text' => 'Since purchasing the XP Micro the other day I felt compelled to write and just give a huge thanks to all who worked to make this product possible. I have not felt this much deep penetrating, relaxing and pain relief in the 5 years since my injury. Thank you again from the bottom of my heart I really feel this will literally put the bounce back in my step. Thank you again I’m so very pleased.',
            'isActive' => true,
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ]);

        $this->insert('site_feedback',[
            'username' => 'DENISE',
            'user_avatar' => '',
            'address' => 'LAS VEGAS, NV',
            'text' => 'I bought a Massage XPo at the Fashion Mall in Las Vegas at a kiosk and I love it. I have stopped paying for massage therapy, love the accupressure chart and have reduced my waist line by 1 inch.',
            'isActive' => true,
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('site_feedback');
    }
}
