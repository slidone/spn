<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m170514_142551_create_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(50),
        ]);

        $this->insert('{{%category}}', [
            'title' => 'head massager',
        ]);

        $this->insert('{{%category}}', [
            'title' => 'foot massager',
        ]);

        $this->insert('{{%category}}', [
            'title' => 'handle massager',
        ]);

        $this->insert('{{%category}}', [
            'title' => 'aroma therapy',
        ]);

        $this->insert('{{%category}}', [
            'title' => 'tens unit',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%category}}');
    }
}
