<?php

use yii\db\Migration;

/**
 * Handles the creation of table `description_row_to_settings`.
 */
class m170509_172925_create_description_row_to_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('settings','description', $this->string(200));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('settings', 'description');
    }
}
