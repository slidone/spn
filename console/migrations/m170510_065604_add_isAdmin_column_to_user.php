<?php

use yii\db\Migration;

class m170510_065604_add_isAdmin_column_to_user extends Migration
{

    public function safeUp()
    {
        $this->addColumn('user', 'isAdmin', $this->boolean()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'isAdmin');
    }

}
