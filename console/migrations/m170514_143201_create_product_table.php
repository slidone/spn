<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m170514_143201_create_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(100)->notNull(),
            'price' => $this->float()->notNull(),
            'description' => $this->text(),
            'category_id' => $this->integer(),
            'main_image' => $this->string(255),
            'gallery' => $this->text(),
            'showSlider' => $this->boolean()->notNull()->defaultValue(0),
            'isAvailable' => $this->boolean()->notNull()->defaultValue(0),
            'isPopular' => $this->boolean()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-product-category_id', '{{%product}}', 'category_id');
        $this->createIndex('idx-product-active', '{{%product}}', 'isAvailable');

        $this->addForeignKey('FK_product_category', '{{%product}}', 'category_id', '{{%category}}', 'id', 'SET NULL');

        $this->insert('{{%product}}',[
            'title' => 'IQ Массажер Pro 6',
            'price' => 799.00,
            'description' => 'Два выхода , который имеет переключатель A и B и имеет 18 режимов массажа',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ]);

        $this->insert('{{%product}}',[
            'title' => 'DUAL TAPPER',
            'price' => 60.00,
            'description' => 'The Dual Tapper is ideal for use on the neck, waist, legs, shoulders, arms, hips, or knees.  Relieves tension and stress! The ',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ]);

        $this->insert('{{%product}}',[
            'title' => 'DAIWA LEGACY',
            'price' => 8000.00,
            'description' => 'THE LONGEST STROKE OF ROLLER MASSAGE Experience and indulge yourself with a 49 inch stroke of massage ',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%product}}');
    }
}
