<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center"><?= \yii\bootstrap\Html::decode($model->name)?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" >
            <?= \yii\bootstrap\Html::decode($model->content)?>
        </div>
    </div>
</div>