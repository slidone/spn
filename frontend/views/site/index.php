<?php

/* @var $this yii\web\View */

?>
<section class="popular_products">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3>Most popular products</h3>
            </div>
        </div>
        <div class="row">
            <?= \frontend\components\ProductWidget::widget(['type' => 'popular'])?>
        </div>
    </div>
</section>
<section class="instant">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center"><?= \yii\bootstrap\Html::encode(Yii::$app->settings->indexTextHeader) ?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <p class="text-center">
                    <?= \yii\bootstrap\Html::encode(Yii::$app->settings->indexTextText) ?>
                </p>
            </div>
        </div>
    </div>
</section>
<section class="home_feedback">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center">HERE’S HOW PEOPLE EVERYWHERE ARE TAKING ADVANTAGE OF HIDOW IN EVERYDAY LIVES</h3>
            </div>
        </div>
        <div class="row">
            <?= \frontend\components\FeedbackWidget::widget(['feedback' => $feedbacks])?>
        </div>
    </div>
</section>
