<?
use \yii\bootstrap\NavBar;
use \yii\bootstrap\Nav;
use \yii\bootstrap\Html;
use \frontend\components\PagesWidget;

?>
<header>
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/img/logo.png'). Yii::$app->settings->siteName,
        'brandUrl' => Yii::$app->homeUrl,
        'brandOptions' => [
            'id' => 'head_logo',
            'class' => 'navbar-brand'
        ],
        'options' => [
            'class' => 'navbar-inverse navbar-static-top navbar',
        ],
    ]);
    $cart = !Yii::$app->session->get('cart.qty') ?  'Cart is empty' : Yii::$app->session->get('cart.qty').' items: $ '.Yii::$app->session->get('cart.sum');
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'Store', 'url' => ['/shop/index']],
    ];

    $model = \common\models\StaticPages::findAll(['isActive' => true, 'position' => 'header']);
    foreach ($model as $item => $value)
    {
        $menuItems[] = ['label' => $value['name'], 'url' => ['/site/page', 'url'=> $value['short_url']]];
    }

    $menuItems[] = [
        'label' => '<span class="fa fa-shopping-cart"></span> '. $cart,
        'url' => '#',
        'encode' => false,
        'linkOptions' => [
            'class'=> 'cart-button',
            'onclick' => 'getCart()'
        ]
    ];
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
    <?
    if($renderSlider):
    ?>
    <div class="owl-carousel owl-theme">
        <?= \frontend\components\SliderWidget::widget()?>
    </div>
    <?endif;?>
</header>