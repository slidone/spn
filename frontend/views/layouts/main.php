<?
use frontend\assets\AppAsset;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="<?= Yii::$app->language ?>"> <!--<![endif]-->
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= \yii\bootstrap\Html::encode(Yii::$app->settings->siteName) ?></title>
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrapper">
    <?
    Yii::$app->controller->route == 'site/index' ? $renderSlider = true : $renderSlider = false;
    echo $this->render('header',['renderSlider'=> $renderSlider])
    ?>
    <div class="content">
            <?= $content ?>
    </div>

    
</div>
<footer class="main_footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-2">
                    <div class="footer_logo">
                        <a href="#"><?= \yii\bootstrap\Html::img('@web/img/logo.png')?> <span>Web Store</span></a>
                    </div>
                </div>
                <div class="col-md-5 col-md-offset-4 col-sm-8 col-xs-10 text-right">


                    <ul>
                        <?
                        $model = \common\models\StaticPages::findAll(['isActive' => true, 'position' => 'footer']);
                        foreach ($model as $item => $value):
                        ?>
                            <li><a href="<?=\yii\helpers\Url::to(['/site/page', 'url' => $value['short_url']])?>"><?=$value['name']?></a></li>
                        <?endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

<?
\yii\bootstrap\Modal::begin([
    'header' => '<h4 class="modal-title" >Your cart</h4>',
    'id' => 'cart',
    //'size' => 'modal-lg',
    'footer' => '
        <a href="'.\yii\helpers\Url::to(['shop/checkout']).'" button" class="btn btn-warning">Proceed to checkout</a>',
]);

\yii\bootstrap\Modal::end();
?>
<div id="blueimp-gallery" class="blueimp-gallery">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
<!--[if lt IE 9]>
<script src="libs/html5shiv/es5-shim.min.js"></script>
<script src="libs/html5shiv/html5shiv.min.js"></script>
<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
<script src="libs/respond/respond.min.js"></script>
<![endif]-->



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>