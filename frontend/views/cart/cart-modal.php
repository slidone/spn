<?php
$session = Yii::$app->session;
if (!empty($session->get('cart'))): ?>
<table class="table table-stripped table-responsive">
    <thead>
        <tr>
            <th></th>
            <th>Item</th>
            <th>Quantity</th>
            <th>Price</th>
            <th></th>
        </tr>
    </thead>
    <tbody>

        <?foreach ($session->get('cart') as $id => $item):?>
        <tr>
            <td>

                <?= \yii\helpers\Html::img(
                    Yii::$app->params['uploadsRoot'].$item['img'],
                    [
                    'alt'=> $item['name'],
                    'height'=>50
                    ]
                    )
                    ?>
                </td>
                <td><?= \yii\helpers\Html::a($item['name'], ['shop/product', 'id' => $id])?></td>
                <td><?= $item['qty']?></td>
                <td>$ <?= $item['price']?></td>
                <td><a data-id="<?=$id?>" style="cursor: pointer;" class="cart-action del-item" href="#">Delete</a></td>
            </tr>
            <?endforeach;?>
        </tbody>
    </table>
</tbody>
</table>
<?else:?>
    <p class="text-center">Cart is empty</p>
<?endif;?>
