<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
?>
<section class="container shop-category">
	<?=$this->render('_filters', ['categories' => $categories])?>
	<div class="row">
		<div class="col-md-12">
			<h3 class="text-center"><?=$category_name?></h3>
		</div>
	</div>
	<div class="row">
		<?= \yii\widgets\ListView::widget([
			'dataProvider' => $dataProviderProduct,
			'itemView' => '_product',
			'layout' => "{items}\n{pager}",
		]);?>
	</div>
	</div>
</section>