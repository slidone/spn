<section>
	<div class="container">
		<div class="product_detail">
			<div class="row ">
				<div class="col-md-4">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<a href="<?=Yii::$app->params['uploadsRoot'].$product->main_image?>" title="<?=$product->title?>" data-gallery="#blueimp-gallery">
								<img src="<?=Yii::$app->params['uploadsRoot'].$product->main_image?>" alt="<?=$product->title?>">
							</a>
						</div>
						<?
						if ($product->gallery):
						foreach (unserialize($product->gallery) as $item):?>
							<div class="col-md-3 col-xs-3">
								<a href="<?=Yii::$app->params['uploadsRoot'].$item?>" title="<?=$product->title?>" data-gallery="#blueimp-gallery">
									<img class="product_detail_img_sub" src="<?=Yii::$app->params['uploadsRoot'].$item?>" alt="<?=$product->title?>">
								</a>
							</div>
						<?endforeach;?>
						<?endif;?>
					</div>
				</div>
				<div class="col-md-8">
					<h4><?=$product->title?></h4>
					<span class="h4">$ <?=$product->price?></span>
					<div class="product_detail_descr">
						<p><?=$product->description?></p>
						<?if(\common\models\Value::find()->where(['product_id' => $product->id])->all()):?>
							<p>
							<?= \yii\grid\GridView::widget([
								'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $product->getValues()]),
								'layout' => "{items}\n{pager}",
								'showHeader' => 'false',
								'columns' => [
									[
										'label' => 'Attribute',
										'attribute' => 'attribute_id',
										'value' => 'productAttribute.name',
									],
									[
										'label' => 'Value',
										'attribute' => 'value',
									],

								],
							]); ?>
							</p>
						<?endif;?>
					</div>
					<form class="form-inline add_cart_form">
						<div class="form-group">
							<input type="email" id="qty_<?=$product->id?>" class="form-control" placeholder="1">
							<a href="<?=\yii\helpers\Url::to(['cart/add', 'id'=> $model->id])?>" data-id="<?=$product->id?>" class="btn btn-warning product_detail_add add-to-cart" role="button"> Add to cart</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="text-center">Reviews for Dual Taper</h3>
					</div>
				</div>
				<div class="row">
					<?= \frontend\components\FeedbackWidget::widget(['feedback' => $feedbacks, 'tpl' => 'product'])?>
				</div>
			</div>
		</section>