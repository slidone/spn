<?
use yii\helpers\Html;
use \yii\bootstrap\Nav;
?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <h4 class="filter_name">Categories</h4>
        <ul class="filters">
            <?$menuItems[] = ['label' => 'All products', 'url' => ['shop/index'], 'linkOptions' => ['class'=> 'btn btn-primary']];?>
            <?foreach($categories as $category):?>
                <?
                $menuItems[] = [
                    'label' => $category->title,
                    'url' => ['shop/category',
                        'id' => $category->id],
                    'linkOptions' => ['class'=> 'btn btn-primary']];
                ?>
                <!-- <li><?=Html::a($category->title,['category', 'id' => $category->id],['class'=> 'btn btn-primary'])?></li> -->
            <?endforeach;?>
            <?
            echo Nav::widget([
                'items' => $menuItems,
            ]);
            ?>
        </ul>
    </div>
</div>