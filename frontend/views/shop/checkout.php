<?$session = Yii::$app->session;?>
<section>
	<div class="container">
		<div class="row" id="cart">
			<div class="col-md-8">
				<div class="thumbnail">
					<?=$this->render('/cart/cart-modal')?>
					<p class="cart_total text-right"><strong>Total: $ <?=$session->get('cart.sum')?></strong></p>
				</div>
			</div>
			<div class="col-md-4">
				<?php $form = \yii\bootstrap\ActiveForm::begin([
					'fieldConfig' => [
						'template' => "{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}"
				]]); ?>

				<?= $form->field($model, 'firstname')->textInput(['placeholder' => 'First Name']) ?>
				<?= $form->field($model, 'lastname')->textInput(['placeholder' => 'Last Name'])  ?>
				<?= $form->field($model, 'email')->input('email')->textInput(['placeholder' => 'email'])  ?>
				<?= $form->field($model, 'phone')->textInput(['placeholder' => 'Phone'])  ?>
				<?= $form->field($model, 'address')->textInput(['placeholder' => 'Address'])  ?>
				<?= $form->field($model, 'country')->textInput(['placeholder' => 'Country'])  ?>
				<?= $form->field($model, 'state')->textInput(['placeholder' => 'State']) ?>
				<?= $form->field($model, 'zip')->textInput(['placeholder' => 'zip']) ?>

				<div class="form-group">
					<?= \yii\bootstrap\Html::submitButton(Yii::t('app', 'Checkout'), ['class' => 'btn btn-warning btn-block']) ?>
				</div>
				<?php \yii\bootstrap\ActiveForm::end(); ?>
<!--				<form>-->
<!--					<div class="form-group">-->
<!--						<div class="row">-->
<!--							<div class="col-md-12">-->
<!--								<input type="email" class="form-control" placeholder="Email">-->
<!--							</div>-->
<!--						</div>-->
<!--					</div>-->
<!--					<div class="form-group">-->
<!--						<div class="row">-->
<!--							<div class="col-md-6">-->
<!--								<input type="text" class="form-control"  placeholder="firstname">-->
<!--							</div>-->
<!--							<div class="col-md-6">-->
<!--								<input type="text" class="form-control"  placeholder="lastname">-->
<!--							</div>-->
<!--						</div>-->
<!--					</div>-->
<!--					<div class="form-group">-->
<!--						<div class="row">-->
<!--							<div class="col-md-12">-->
<!--								<input type="text" class="form-control" placeholder="adress">-->
<!--							</div>-->
<!--						</div>-->
<!--					</div>-->
<!--					<div class="form-group">-->
<!--						<div class="row">-->
<!--							<div class="col-md-6">-->
<!--								<input type="text" class="form-control"  placeholder="country">-->
<!--							</div>-->
<!--							<div class="col-md-6">-->
<!--								<input type="text" class="form-control"  placeholder="state">-->
<!--							</div>-->
<!--						</div>-->
<!--					</div>-->
<!--					<div class="form-group">-->
<!--						<div class="row">-->
<!--							<div class="col-md-6">-->
<!--								<input type="text" class="form-control"  placeholder="ZIP">-->
<!--							</div>-->
<!--							<div class="col-md-6">-->
<!--								<input type="phone" class="form-control"  placeholder="Phone">-->
<!--							</div>-->
<!--						</div>-->
<!--					</div>-->
<!--					<button type="submit" class="btn btn-warning btn-block">Submit</button>-->
<!--				</form>-->
			</div>
		</div>
	</div>
</section>