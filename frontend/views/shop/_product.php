<?
use yii\bootstrap\Html;
use yii\helpers\Url;
?>

<div class="col-md-3 col-sm-6 col-xs-12 product">
	<div class="thumbnail">
		<div class="product_image">
		<?= Html::a(
                Html::img(Yii::$app->params['uploadsRoot'].$model->main_image),
                Url::to([
                    '/shop/product',
                    'id' => $model->id
                ])
                )?>
		</div>
		<div class="product_content">
			<p class="pop_product_name text-center"><a href="<?=Url::to(['shop/product' , 'id'=> $model->id])?>"><?= $model->title?></a></p>
			<p class="pop_product_price text-center">$ <?=$model->price?></p>
			<?= Html::a(
                'View Detail',
                Url::to([
                    '/shop/product',
                    'id' => $model->id
                ]),
                [
                    'class' => 'btn btn-block btn-warning',
                ])?>
		</div>
	</div>
</div>