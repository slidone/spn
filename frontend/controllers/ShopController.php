<?php

namespace frontend\controllers;
use common\models\Category;
use common\models\Order;
use common\models\Product;
use common\models\ProductFeedback;
use common\models\search\ProductSearch;
use frontend\models\ChechoutForm;
use yii\base\Exception;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class ShopController extends \yii\web\Controller
{
    public function actionIndex($sort='')
    {
    	$categories = Category::find()->all();
    	$productSearch = new ProductSearch();
    	if (!$sort) {	
    		$filter = ['created_at' => SORT_ASC];
    	}
    	elseif ($sort == 'desc') {
    		$filter = ['price' => SORT_DESC];
    	}
    	elseif ($sort == 'asc') {
    		$filter = ['price' => SORT_ASC];
    	}

    	$dataProviderProduct = new ActiveDataProvider([
            'query' => Product::find()->active(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
            	'defaultOrder' => $filter,
            ],
        ]);
        return $this->render('index', [
        	'categories' => $categories,
        	'dataProviderProduct' => $dataProviderProduct,
        	'productSearch' => $productSearch,
        ]);
    }

   public function actionProduct($id)
   {
	   $this->checkParam($id);
   		$feedbacks = ProductFeedback::find()->where(['product_id' => $id])->limit(4)->all();
   		$product = Product::findOne(['id' => $id]);
		$values = $product->getValues();

   		return $this->render('product', [
        	'product' => $product,
			'values' => $values,
        	'feedbacks' => $feedbacks,
        ]);
   }

   public function actionCategory($id)
   {
	   $this->checkParam($id);
   		$categories = Category::find()->all();
   		$category_name = Category::findOne(['id' => $id])->title;
   		$dataProviderProduct = new ActiveDataProvider([
            'query' => Product::find()->where(['category_id' => $id]),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $this->render('category', [
        	'category_name' => $category_name,
        	'categories' => $categories,
        	'dataProviderProduct' => $dataProviderProduct,
        ]);
   }

   public function actionCheckout()
   {
	   $model = new Order();
	   
	   $post = \Yii::$app->request->post();
	   
	   if ($model->load($post) && $model->validate())
	   {
		   if ($model->save())
		   {
			   \Yii::$app->session->setFlash('success', 'We call you!');
			   
		   }
	   }
	   

	   return $this->render('checkout', ['model'=> $model]);
   }
	private function checkParam($id)
	{
		if (!is_numeric($id) || $id < 0)
		{
			throw new NotFoundHttpException('Page not found');
		}
	}
}
