<?php

namespace frontend\controllers;
use common\models\Cart;
use common\models\Product;
use Yii;

class CartController extends \yii\web\Controller
{

    public function actionAdd($id, $qty=null)
    {
        if (!Yii::$app->request->isAjax)
        {
            return $this->redirect(Yii::$app->request->referrer);
        }

        $qty = !(int)$qty ? 1 : (int)$qty;
        $item = Product::findOne($id);
        if (!$item || empty($item))
        {
            return false;
        }

        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->addToCart($item, $qty);


        return $this->renderPartial('cart-modal', compact('session'));
    }

    public function actionDelItem($id)
    {
        $id = Yii::$app->request->get('id');
        $session =Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->recalc($id);
        $this->layout = false;
        return $this->render('cart-modal', compact('session'));
    }
    
    public function actionClear()
    {
        if (!Yii::$app->request->isAjax)
        {
            return $this->redirect(Yii::$app->request->referrer);
        }
        $session = Yii::$app->session;
        $session->open();
        $session->remove('cart');
        $session->remove('cart.qty');
        $session->remove('cart.sum');
        $this->layout = false;
        return $this->render('cart-modal', compact('session'));
    }

    public function actionShow()
    {
        if (!Yii::$app->request->isAjax)
        {
            return $this->redirect(Yii::$app->request->referrer);
        }
        $this->layout = false;
        $session = Yii::$app->session;
        return $this->render('cart-modal', compact('session'));
    }
}
