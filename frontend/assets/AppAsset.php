<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'libs/animate/animate.css',
        'libs/carousel/css/owl.carousel.min.css',
        'libs/carousel/css/owl.theme.default.min.css',
        'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css',
        'css/fonts.css',
        'css/theme.css',
        'css/media.css',
        'libs/gallery/css/blueimp-gallery.min.css',
    ];
    public $js = [
        'libs/waypoints/waypoints.min.js',
        'libs/animate/animate-css.js',
        'libs/plugins-scroll/plugins-scroll.js',
        'libs/carousel/js/owl.carousel.min.js',
        'libs/gallery/js/jquery.blueimp-gallery.min.js',
        'js/common.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
