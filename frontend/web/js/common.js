$(document).ready(function() {
	//Chrome Smooth Scroll
	try {
		$.browserSelector();
		if($("html").hasClass("chrome")) {
			$.smoothScroll();
		}
	} catch(err) {

	};

	$('#head_logo').animated("slideInDown", "fadeOut");
	$('.instant h2').animated("rubberBand", "fadeOut");
	$('.instant p').animated("bounceIn", "fadeOut");
	
	var header_slider = $(".owl-carousel");
	header_slider.owlCarousel({
		center:true,
		items: 1,
		dots: true,
		autoplay:true,
		autoplayTimeout:7000,
		autoplayHoverPause:true,
		autoplaySpeed: 1000,
		rewind:true,
		animateIn: 'bounceIn',
		onInitialize: initOwl,
	});

	header_slider.on('changed.owl.carousel',function(event) {
		var id = event.item.index;
		var target = event.target.children[0].children[0].children[id];
		var bg_img_name = $(target).find('.background-image > img').attr('src');
		setHeaderBg(bg_img_name);	
	});

	function initOwl() {
		var firstImg = $('header .background-image:first > img').attr('src');
		setHeaderBg(firstImg);
	}

	function setHeaderBg(img_name) {
		var header = $('header');
		var gradient = 'rgba(68, 57, 100, .9), rgba(197,65,104, .9)';
		var styles = {};
		if (img_name === undefined) {
			styles = {
				'background' : 'linear-gradient('+gradient+')',
				'-webkit-background-size': 'cover',
				'background-size' : 'cover',
			};
		}
		else {
			styles = {
				'background' : 'linear-gradient('+gradient+'), url('+img_name+")",
				'-webkit-background-size': 'cover',
				'background-size' : 'cover',
			};
		}
		header.css(styles);
	}
function showCart(cart) {
    $('#cart .modal-body').html(cart);
    $('#cart').modal();
}

function getCart() {
    $.ajax({
        url: '/cart/show',
        type: 'GET',
        success: function(res){
            if(!res) alert('Ошибка!');
            showCart(res);
        },
        error: function(){
            alert('Error!');
        }
    });
}

function updateButton() {
    $(".cart-button").load("/ .cart-button");
}

    function updateCheck() {
        $("#cart .thumbnail").load("/ #cart .thumbnail");
    }

function clearCart() {
    $.ajax({
        url: '/cart/clear',
        type: 'GET',
        success: function(res){
            if(!res) alert('Ошибка!');
            showCart(res);
            updateButton();
        },
        error: function(){
            alert('Error!');
        }
    });
}

$('#cart').on('click', '.del-item', function () {
    var id = $(this).data('id');
    $.ajax({
        url: '/cart/del-item',
        data: {id: id},
        type: 'GET',
        success: function(res){
            if(!res) alert('Ошибка!');
            updateButton();
            updateCheck();
            updateCheck();
        },
        error: function(){
            alert('Error!');
        }
    });
});
    $('#cart .modal-body').on('click', '.del-item', function () {
        var id = $(this).data('id');
        $.ajax({
            url: '/cart/del-item',
            data: {id: id},
            type: 'GET',
            success: function(res){
                if(!res) alert('Ошибка!');
                showCart(res);
                updateButton();
                updateCheck();
            },
            error: function(){
                alert('Error!');
            }
        });
    });
$('.add-to-cart').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id'),
        qty = $('#qty_'+id).val();
    $.ajax({
        url: '/cart/add',
        data: {id:id, qty:qty},
        type: 'GET',
        success: function(res){
            if(!res) alert('Ошибка!');
            showCart(res);
            updateButton();
            updateCheck()
        },
        error: function(){
            alert('Error!');
        }
    });
});

$('.cart-button').on('click', function (e) {
    e.preventDefault();
    getCart();
    updateButton();
});

    var gallery = $('#blueimp-gallery').data('gallery');
});