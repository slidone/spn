<?php
/**
 * Created by PhpStorm.
 * User: Владислав
 * Date: 14.05.2017
 * Time: 15:33
 */

namespace frontend\components;

use yii\base\Widget;
use common\models\Product;

class ProductWidget extends Widget
{
    public $products;
    public $type = '';

    public function init()
    {
        parent::init();

    }

    public function run()
    {
        parent::run();
        return $this->renderSlider(Product::find()->where([ $this->generateAttr() => true])->limit(4)->all());
    }

    protected function renderSlider($products)
    {
        ob_start();
        include __DIR__ . '/product_layout/' . $this->setTpl() . '.php';
        return ob_get_clean();
    }

    private function generateAttr()
    {
        if ($this->setTpl() == 'popular') {
           return 'isPopular';
        }
    }

    private function setTpl()
    {
        if (!$this->type) 
        {
            return $this->type = 'index';
        }
        else
        {
            return $this->type;
        }
    }
}