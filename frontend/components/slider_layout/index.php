<?
use \yii\bootstrap\Html;
?>
<?foreach ($products as $product):?>
<section class="header_section">
            <div class="container">
                <div class="row">
                    <div class="background-image" style="display: none;">
                        <?=Html::img(Yii::$app->params['uploadsRoot']. $product->main_image)?>
                    </div>
                    <div class="col-md-3 col-md-offset-1 col-sm-6 col-xs-12">
                        <div class="header_img">
                            <?= Html::a(
                            Html::img(Yii::$app->params['uploadsRoot']. $product->main_image, ['style' => 'width: auto;height: 250px;']),
                            ['shop/product', 'id' => $product->id]
                            )?>
                        </div>
                    </div>
                    <div class="col-md-7 col-md-offset-1 col-sm-6 col-xs-12">
                        <div class="header_content text-right">
                            <h1><?=$product->title?></h1>
                            <p><?=$product->description?></p>
                            <?=Html::a('Shop now <span class="fa fa-shopping-cart"></span>', ['shop/product', 'id' => $product->id], ['class'=> 'btn btn-warning'])?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<?endforeach;?>