<?php
/**
 * Created by PhpStorm.
 * User: Владислав
 * Date: 14.05.2017
 * Time: 15:33
 */

namespace frontend\components;

use yii\base\Widget;
use common\models\Product;
class SliderWidget extends Widget
{
    public $products;
    public $tpl = '';

    public function init()
    {
        parent::init();

    }

    public function run()
    {
        parent::run();
        return $this->renderSlider(Product::findAll(['showSlider'=> true]));
    }

    protected function renderSlider($products)
    {
        ob_start();
        include __DIR__ . '/slider_layout/' . $this->setTpl() . '.php';
        return ob_get_clean();
    }

    private function setTpl()
    {
        if (!$this->tpl) 
        {
            return $this->tpl = 'index';
        }
        else
        {
            return $this->tpl;
        }
    }
}