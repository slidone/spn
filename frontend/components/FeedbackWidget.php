<?php
/**
 * Created by PhpStorm.
 * User: Владислав
 * Date: 14.05.2017
 * Time: 15:33
 */

namespace frontend\components;

use yii\base\Widget;

class FeedbackWidget extends Widget
{
    public $feedback;
    public $tpl = '';

    public function init()
    {
        parent::init();

    }

    public function run()
    {
        parent::run();
        return $this->renderFeedback($this->feedback);
    }

    protected function renderFeedback($feedbacks)
    {
        if (!$feedbacks)
        {
            return '<span class="text-center">No feedback`s</span> ';
        }
        ob_start();
        include __DIR__ . '/feedback_layout/' . $this->setTpl() . '.php';
        return ob_get_clean();
    }

    private function setTpl()
    {
        if (!$this->tpl) 
        {
            return $this->tpl = 'index';
        }
        else
        {
            return $this->tpl;
        }
    }
}