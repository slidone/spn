<?foreach ($feedbacks as $item=>$value):?>
<div class="col-md-3">
    <div class="feedback">
        <div class="feedback_head">
            <?= \yii\bootstrap\Html::img(Yii::$app->params['uploadsRoot']. $value->user_avatar)?>
            <ul>
                <li><?=$value->username?></li>
                <li><?=$value->address?></li>
            </ul>
        </div>
        <div class="feedback_content">
            <p><?=\yii\bootstrap\Html::decode($value->text) ?></p>
        </div>
    </div>
</div>
<?endforeach;?>