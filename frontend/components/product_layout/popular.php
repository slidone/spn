<?
use \yii\bootstrap\Html;
?>
<?foreach ($products as $product):?>
<div class="col-md-3 col-sm-6 col-xs-12">
    <div class="thumbnail">
        <?= Html::a(
                            Html::img(Yii::$app->params['uploadsRoot']. $product->main_image),
                            ['shop/product', 'id' => $product->id]
                            )?>
        <p class="pop_product_name text-center"><?= Html::a($product->title,['shop/product', 'id' => $product->id]);?></p>
        <p class="pop_product_price text-center">$ <?= $product->price?></p>
        <?= Html::a('View Detail', ['shop/product', 'id' => $product->id], ['class'=> 'btn btn-block btn-warning']);?>
    </div>
</div>
<?endforeach;?>