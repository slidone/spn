<?php
/**
 * Created by PhpStorm.
 * User: Владислав
 * Date: 29.05.2017
 * Time: 22:36
 */

namespace frontend\models;


use common\models\Cart;
use common\models\Order;
use yii\base\Model;

class ChechoutForm extends Model
{
    public $firstname;
    public $lastname;
    public $email;
    public $phone;
    public $address;
    public $country;
    public $state;
    public $zip;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'email', 'phone', 'address', 'country', 'state', 'zip'], 'required'],
            [['phone'], 'integer'],
            [['firstname', 'lastname', 'email', 'country', 'state'], 'string', 'max' => 50],
            [['address'], 'string', 'max' => 200],
            [['zip'], 'string', 'max' => 10],
        ];
    }
    
    public function saveOrder()
    {
        $order = new Order();
        $cart = new Cart();

        $order->firstname = $this->firstname;
        
    }

}