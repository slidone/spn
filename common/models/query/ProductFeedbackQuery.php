<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\ProductFeedback]].
 *
 * @see \common\models\ProductFeedback
 */
class ProductFeedbackQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ProductFeedback[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere(['isActive' => true]);
    }
    
    /**
     * @inheritdoc
     * @return \common\models\ProductFeedback|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
