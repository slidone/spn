<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property integer $phone
 * @property string $address
 * @property string $country
 * @property string $state
 * @property string $zip
 *
 * @property OrderProducts[] $orderProducts
 * @property Product[] $products
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'email', 'phone', 'address', 'country', 'state', 'zip'], 'required'],
            [['phone'], 'integer'],
            [['firstname', 'lastname', 'email', 'country', 'state'], 'string', 'max' => 50],
            [['address'], 'string', 'max' => 200],
            [['zip'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'firstname' => Yii::t('app', 'Firstname'),
            'lastname' => Yii::t('app', 'Lastname'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'address' => Yii::t('app', 'Address'),
            'country' => Yii::t('app', 'Country'),
            'state' => Yii::t('app', 'State'),
            'zip' => Yii::t('app', 'Zip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProducts()
    {
        return $this->hasMany(OrderProducts::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('order_products', ['order_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\OrderQuery(get_called_class());
    }


    public function saveProducts($orderId)
    {
        $orderItemsModel = new OrderProducts();
        $cart = new Cart();

        foreach ($cart->getItems() as $item => $value)
        {
            $orderItemsModel->product_id = $item;
            $orderItemsModel->order_id = $orderId;
            $orderItemsModel->insert();
        }
    }
}
