<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ProductFeedback;

/**
 * ProductFeedbackSearch represents the model behind the search form about `common\models\ProductFeedback`.
 */
class ProductFeedbackSearch extends ProductFeedback
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'isActive'], 'integer'],
            [['username', 'product_id','user_avatar', 'text'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductFeedback::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            //'product_id' => $this->product_id,
            'isActive' => $this->isActive,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'user_avatar', $this->user_avatar])
            ->andFilterWhere(['like', 'text', $this->text]);

        $query->joinWith(['product' => function ($q) {
            $q->where('product.title LIKE "%' . $this->product_id . '%"');
        }]);

        return $dataProvider;
    }
}
