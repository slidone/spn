<?php
namespace common\models;


use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\BaseFileHelper;
use yii\imagine\Image;
use yii\web\UploadedFile;

class UploadImageForm extends Model
{

    public $file;
    public $files;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, png'],
            [['files'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxFiles' => 10],
        ];
    }

    public function uploadFile(UploadedFile $file, $currentFile, $currentFolder)
    {
        $this->file = $file;
        $rootFolder = Yii::getAlias('@frontend/web/uploads/');
        //if (!$this->validate()) return false;

        if (!$currentFolder)
        {
            BaseFileHelper::createDirectory($rootFolder . $this->generatePath());
            $filename = $this->generatePath() . $this->generateFilename();
        }
        else
        {
            BaseFileHelper::createDirectory($rootFolder . $currentFolder);
            $filename = $currentFolder . $this->generateFilename();
        }

        if ($currentFile && $this->fileExist($rootFolder.$currentFile))
        {
            $this->deleteCurrImage($rootFolder , $currentFile);
        }

        $this->file->saveAs($rootFolder . $filename);
        $this->setQualityImage($rootFolder . $filename);

        return $filename;
    }

    public function uploadFiles($files, $currentFile, $currentFolder)
    {
        //if (!$this->validate()) return false;
        $filesArray = [];
        $rootFolder = Yii::getAlias('@frontend/web/uploads/');

        if ($currentFile && $this->fileExist($rootFolder.$currentFile))
        {
            $currFileArray = unserialize($currentFile);
            foreach ($currFileArray as $currFile)
            {
                $this->deleteCurrImage($rootFolder, $currFile);
            }
        }

        
        foreach ($files as $file)
        {
            if (!$currentFolder)
            {
                BaseFileHelper::createDirectory($rootFolder . $this->generatePath());
                $filename = $this->generatePath() . $this->generateFilename();
            }
            else
            {
                BaseFileHelper::createDirectory($rootFolder . $currentFolder);
                $filename = $currentFolder . strtolower(md5(uniqid($file->baseName)) . '.' . $file->extension);
            }

            $file->saveAs($rootFolder . $filename);
            $this->setQualityImage($rootFolder . $filename);

            array_push($filesArray, $filename);
        }        
        
        return $filesArray;
    }

    private function fileExist($file)
    {
        if (!empty($file) && $file != null)
        {
            return file_exists($file);
        }
        return false;
    }

    private function generateFilename()
    {
        return strtolower(md5(uniqid($this->file->baseName)) . '.' . $this->file->extension);
    }

    private function generatePath()
    {
        return sprintf('/%s/%s', Yii::$app->params['image_folder'], date('Y/m/d/'));
    }

    public function deleteCurrImage($rootFolder,$file)
    {
        if(!$file) return false;
        if ($this->fileExist($rootFolder . $file))
        {
            @unlink($rootFolder . $file);
        }
    }

    private function setQualityImage($file)
    {
        if($this->fileExist($file))
        {
            Image::getImagine()->open($file)->thumbnail(new Box(300,300))->save($file, ['quality' => 80]);
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Image'),
        ];
    }

    public function deleteSmth($folder)
    {
        if(is_dir($folder)){
            $files = glob( $folder . '*', GLOB_MARK );

            foreach($files as $file)
            {
                $this->deleteSmth( $file );
            }

            @rmdir($folder);
        }
        elseif(is_file($folder))
        {
            @unlink($folder);
        }
    }

}