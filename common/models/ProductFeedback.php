<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_feedback".
 *
 * @property integer $id
 * @property string $username
 * @property string $user_avatar
 * @property string $text
 * @property integer $product_id
 * @property integer $isActive
 *
 * @property Product $product
 */
class ProductFeedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_feedback}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'text', 'product_id'], 'required'],
            [['text'], 'string'],
            [['product_id', 'isActive'], 'integer'],
            [['username'], 'string', 'max' => 50],
            [['user_avatar'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'user_avatar' => Yii::t('app', 'User Avatar'),
            'text' => Yii::t('app', 'Text'),
            'product_id' => Yii::t('app', 'Product Title'),
            'isActive' => Yii::t('app', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\ProductFeedbackQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ProductFeedbackQuery(get_called_class());
    }

    public function saveUserAvatar($filename)
    {
        if ($filename && !is_null($filename))
        {
            $this->user_avatar = $filename;
            return $this->save(false);
        }
    }
}
