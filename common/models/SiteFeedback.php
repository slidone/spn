<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%site_feedback}}".
 *
 * @property integer $id
 * @property string $username
 * @property string $user_avatar
 * @property string $address
 * @property string $text
 * @property integer $isActive
 * @property integer $created_at
 * @property integer $updated_at
 */
class SiteFeedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_feedback}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'address', 'text'], 'required'],
            [['text'], 'string'],
            [['isActive', 'created_at', 'updated_at'], 'integer'],
            [['username', 'address'], 'string', 'max' => 50],
            [['user_avatar'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => date('U'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'user_avatar' => Yii::t('app', 'User Avatar'),
            'address' => Yii::t('app', 'Address'),
            'text' => Yii::t('app', 'Text'),
            'isActive' => Yii::t('app', 'Is Active'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function saveUserAvatar($filename)
    {
        $this->user_avatar = $filename;
        return $this->save(false);
    }

    public function beforeDelete()
    {
        $this->deleteImage();
        return parent::beforeDelete();
    }

    public function deleteImage()
    {
        $uploadImageForm = new UploadImageForm();
        $uploadImageForm->deleteCurrImage(Yii::getAlias('@frontend/web/uploads/'),$this->user_avatar);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\SiteFeedbackQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\SiteFeedbackQuery(get_called_class());
    }
}
