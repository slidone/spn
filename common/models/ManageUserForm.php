<?php

namespace common\models;


class ManageUserForm extends User
{
    public $username;
    public $password;
    public $email;
    public $isAdmin;

    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required', 'on' => 'createUser'],
            ['password', 'string', 'min' => 6],

            ['isAdmin', 'default', 'value' => 0],
            ['isAdmin', 'in', 'range' => [0,1]],
        ];
    }

    /**
     * @return User|null
     */
    public function createUser()
    {
        if (!$this->validate('createUser'))
        {
            return null;
        }

        $userModel = new User();
        $userModel->username = $this->username;
        $userModel->email = $this->email;
        $userModel->setPassword($this->password);
        $userModel->generateAuthKey();

        return $userModel->save() ? $userModel : null;
    }


    public function updateUser($userId)
    {
        if (!$this->validate() || !$userId || empty($userId))
        {
            return null;
        }

        $userModel = User::findOne($userId);
        if ($userModel === null)
        {
            return null;
        }

        $userModel->username = $this->username;
        $userModel->email = $this->email;
        $userModel->setPassword($this->password);
        return $userModel->save() ? $userModel : null;
    }


}