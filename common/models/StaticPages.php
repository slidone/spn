<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%static_pages}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $content
 * @property string $short_url
 * @property integer $isActive
 */
class StaticPages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%static_pages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'content', 'short_url', 'position'], 'required'],
            [['content', 'position'], 'string'],
            [['isActive'], 'integer'],
            [['name', 'short_url'], 'string', 'max' => 200],
            [['short_url'], 'match', 'pattern' => '/^[a-z]\w*$/i', 'message' => 'Must be contained only A-Z and 1-9'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'content' => Yii::t('app', 'Content'),
            'short_url' => Yii::t('app', 'Short Url'),
            'isActive' => Yii::t('app', 'Is Active'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\StaticPagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\StaticPagesQuery(get_called_class());
    }
}
