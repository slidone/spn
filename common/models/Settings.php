<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 * @property integer $r_flag
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'value', 'description'], 'required'],
            [['key'], 'match', 'pattern' => '/^[a-z]\w*$/i', 'message' => 'The key can consist only of A-z and numbers. Spaces are not allowed.'],
            [['key'], 'unique', 'targetClass' => '\common\models\Settings'],
            [['r_flag'], 'integer'],
            [['r_flag'], 'in', 'range' => [0,1]],
            [['key', 'value', 'description'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Key'),
            'value' => Yii::t('app', 'Value'),
            'description' => Yii::t('app', 'Description'),
            'r_flag' => Yii::t('app', 'Type'),
        ];
    }
}
