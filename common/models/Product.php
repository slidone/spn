<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\BaseFileHelper;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property integer $id
 * @property string $title
 * @property double $price
 * @property string $description
 * @property integer $category_id
 * @property string $main_image
 * @property string $gallery
 * @property integer $showSlider
 * @property integer $isAvailable
 * @property integer $isPopular
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property OrderProducts[] $orderProducts
 * @property Order[] $orders
 * @property Category $category
 * @property ProductFeedback[] $productFeedbacks
 * @property Value[] $values
 * @property Attribute[] $attributes
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'price'], 'required'],
            [['price'], 'number', 'min' => 0],
            [['description', 'gallery'], 'string'],
            [['category_id', 'showSlider', 'isAvailable', 'isPopular', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['main_image'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => date('U'),
            ],
        ];
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        BaseFileHelper::removeDirectory(Yii::getAlias('@frontend/web/uploads/products/') . $this->id);
        return parent::beforeDelete();
    }

    /**
     * @param $filename
     * @return bool
     */
    public function saveMainImage($filename)
    {
        if ($filename && !is_null($filename))
        {
            $this->main_image = $filename;
            return $this->save(false);
        }
    }

    /**
     * @param $files
     * @return bool
     */
    public function saveGallery($files)
    {
        if ($files && !is_null($files))
        {
            $this->gallery = serialize($files);
            return $this->save(false);
        }

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'price' => Yii::t('app', 'Price'),
            'description' => Yii::t('app', 'Description'),
            'category_id' => Yii::t('app', 'Category ID'),
            'main_image' => Yii::t('app', 'Main Image'),
            'gallery' => Yii::t('app', 'Gallery'),
            'showSlider' => Yii::t('app', 'Show Slider'),
            'isAvailable' => Yii::t('app', 'Available'),
            'isPopular' => Yii::t('app', 'Popular'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProducts()
    {
        return $this->hasMany(OrderProducts::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['id' => 'order_id'])->viaTable('{{%order_products}}', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductFeedbacks()
    {
        return $this->hasMany(ProductFeedback::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValues()
    {
        return $this->hasMany(Value::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributes()
    {
        return $this->hasMany(Attribute::className(), ['id' => 'attribute_id'])->viaTable('{{%value}}', ['product_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ProductQuery(get_called_class());
    }
}
