<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=spn',
            'username' => 'spn_admin',
            'password' => 'relaxMan',
            'charset' => 'utf8',
        ],
    ],
];
