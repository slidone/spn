<?php
namespace common\components;


use yii\base\Component;
use yii\helpers\ArrayHelper;

class Settings extends Component
{
    private $_attributes;

    public function init()
    {
        parent::init();
        $this->_attributes = ArrayHelper::map(\common\models\Settings::find()->all(), 'key', 'value');
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->_attributes))
        {
            return $this->_attributes[$name];
        }
        return parent::__get($name);
    }
}